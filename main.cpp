#include <iostream>

using namespace std;

int main() {
    int count;
    cin >> count;
    while (count--) {
        int last;
        int current = -1;
        int result = 0;
        int i = -1;
        do {
            i++;
            last = current;
            cin >> current;
            if (current < 0) {
                break;
            }
            if (last < 0) {
                continue;
            }
            if (last == current) {
                if (result == 1) {
                    result = 3;
                } else if (result == 2) {
                    result = 4;
                }
            } else if (last > current) {
                if (result == 0) {
                    if (i == 1) {
                        result = 2;
                    } else {
                        result = 4;
                    }
                } else if (result == 1 || result == 3) {
                    result = 5;
                }
            } else if (last < current) {
                if (result == 0) {
                    if (i == 1) {
                        result = 1;
                    } else {
                        result = 3;
                    }
                } else if (result == 2 || result == 4) {
                    result = 5;
                }
            }
        } while (true);
        switch (result) {
            case 0:
                cout << "staly" << endl;
                break;
            case 1:
                cout << "rosnacy" << endl;
                break;
            case 2:
                cout << "malejacy" << endl;
                break;
            case 3:
                cout << "niemalejacy" << endl;
                break;
            case 4:
                cout << "nierosnacy" << endl;
                break;
            case 5:
                cout << "nieokreslony" << endl;
                break;
        }
    }
    return 0;
}
